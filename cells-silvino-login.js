{
  const {
    html,
  } = Polymer;
  /**
    `<cells-silvino-login>` Description.

    Example:

    ```html
    <cells-silvino-login></cells-silvino-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-silvino-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsSilvinoLogin extends Polymer.Element {

    static get is() {
      return 'cells-silvino-login';
    }

    static get properties() {
      return {
        name:{
          type: String,
          value: ""
        },
        pass:{
          type: String
        },
        info:{
          type: Boolean,
          value: false,
          notify:true
        },
        escondelogin:{
          type: Boolean,
          value: true
        }
      };
    }

    validacionDatos(event){
      event.preventDefault();      
      var usuario = event.target.txtnombreusuario.value;
      var pass = event.target.txtpassword.value;
      if((usuario == "silvino") && (pass == "12345")){
        this.dispatchEvent(new CustomEvent("acceso-correcto",{detail:this.name}))
        // alert("Bienvenido");
        // this.set("info",true);
        // this.set("escondelogin",false);
      }else{
        alert("Verifica los datos");
      }
    }
    static get template() {
      return html `
      <style include="cells-silvino-login-styles cells-silvino-login-shared-styles"></style>
      <slot></slot>
      <div class="frmlogin" >
      <div class="datospersonales">
        <form name="frmdatos" on-submit="validacionDatos" >
          <a>Nombre de usuario:</a>
          <input type="text" name="txtnombreusuario" id="txtnombreusuario" value="{{name::input}}">
          <a>Contraseña:</a>
          <input type="password" name="txtpassword" id="txtpassword" value="{{pass::input}}" >
          <button name="btnsubmit" type="submit">Ingresar</button>
        </form>
      </div>
      </div>
      `;
    }
  }

  customElements.define(CellsSilvinoLogin.is, CellsSilvinoLogin);
   
}