# &lt;cells-silvino-login&gt;

Your component description.

![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
Example:
```html
<cells-silvino-login></cells-silvino-login>
```

## Styling
  The following custom properties and mixins are available for styling:

  ### Custom Properties
  | Custom Property     | Selector | CSS Property | Value       |
  | ------------------- | -------- | ------------ | ----------- |
  | --cells-fontDefault | :host    | font-family  |  sans-serif |
  ### @apply
  | Mixins    | Selector | Value |
  | --------- | -------- | ----- |
  | --cells-silvino-login | :host    | {} |
